#ifndef __ESDSP_ERROR_H
#define __ESDSP_ERROR_H

#include <stdlib.h>

#define ESDSP_ERR_NO_ERROR			 0
#define ESDSP_ERR_NOT_FOUND			-1
#define ESDSP_ERR_NUM_ELEMENTS		-2
#define ESDSP_ERR_NULL_POINTER		-3
#define ESDSP_ERR_VAL_OUT_OF_BOUND	-4



char*	error_explain(int iError);


#endif