// ESDSP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <process.h>
#include <iostream>
#include <math.h>
#include <time.h>
#include <fstream>

//#include "Test.h"

using namespace std;

#include "esdsp.h"


double dSignal[1024] = {0};
void mean_standard_dev_test();
void histogram_test();
void normal_distribution_test();
void noise_test();
void convolution_test();
void dft_test();
void dft_reverse_test();
void rectangular_polar_conversion_test();

ofstream ofile;

int _tmain(int argc, _TCHAR* argv[])
{
	//CppUnit::TextTestRunner runnerText;
	//runnerText.addTest(Test::suite());
	//runnerText.run();

	ofile.open("output.txt");

	double dX_Sample = 0.0;

	for (int ii = 0; ii < 1024; ii++)
	{
		dSignal[ii] = sin(2.0 * PI * 8.0 * dX_Sample);
		//dSignal[ii] = 0;
		dX_Sample+=1.0/32.0;
	}

	//dSignal[0] = 1;

	//mean_standard_dev_test();

	//histogram_test();

	//normal_distribution_test();


	//noise_test();


	//convolution_test();

	dft_test();
	dft_reverse_test();

	//rectangular_polar_conversion_test();

	ofile.close();

	//system("pause");
	return 0;
}

void histogram_test()
{
	double dMin		=	-1.0;
	double dMax		=	1.0;
	unsigned int iNumberElements = 10;

	unsigned int aiBins[10] = {0};

	for (int ii = 0; ii < 1024; ii++)
	{
		histogram_running(aiBins,dSignal[ii],dMin,dMax,iNumberElements);
	}

	for (unsigned int p = 0; p < iNumberElements; p++)
	{
		cout << "Bin " << p << " : " << aiBins[p] << endl;
	}

}

void mean_standard_dev_test()
{
unsigned int iNumberOfElements = 0;

	double dMean = 0, dStdDev = 0,dSum = 0,dSumSquares = 0;

	for (int ii = 0; ii < 1024; ii++)
	{
		dSignal[ii] = sin(2.0 * PI * ii * 1.0/64.0);
	
		mean_stdev_running(&dMean,&dStdDev,&iNumberOfElements,&dSum,&dSumSquares,dSignal[ii]);
	}
	
	cout << "Running mean " << dMean << endl;
	cout << "Running Deviation" << dStdDev << endl;
	
	mean(&dMean, dSignal, 1024);
	standard_deviation(&dStdDev, dSignal, 1024);

	cout << "Computed mean " << dMean << endl;
	cout << "Standard Deviation " << dStdDev << endl;

	double dMeanError;

	if (typical_mean_error(&dMeanError,dStdDev,1024) == ESDSP_ERR_NO_ERROR)
	{
		cout << "Typical mean error: " << dMeanError << endl;
	}

}

void normal_distribution_test()
{
	double dMin		=	-1.0;
	double dMax		=	1.0;


	double dNormalDistribution[1024];

	normal_distribution(dNormalDistribution,(dMax - dMin),dSignal,1024);

	for (unsigned int p = 0; p < 1024; p++)
	{
		cout << "Normal Dist: " << dSignal[p] << " : " << dNormalDistribution[p] << endl;
	}
}

void convolution_test()
{
	double dOutputSignal[1024 + 3  - 1] = {0};
	double dResponseSignal[3] = {-1,0,0};

	//convolution_input(dOutputSignal,dSignal,1024,dResponseSignal,3);

	for (unsigned int p = 0; p < (1024 + 3  - 1); p++)
	{
		if (p < 1024)
			cout << "Convolution " << p << " " << dSignal[p] << "  " << dOutputSignal[p] << endl;
		else
			cout << "Convolution " << p << " " << "            " << dOutputSignal[p] << endl;

	}

	convolution_output(dOutputSignal,dSignal,1024,dResponseSignal,3);

	for (unsigned int p = 0; p < (1024 + 3  - 1); p++)
	{
		if (p < 1024)
			cout << "Convolution " << p << " " << dSignal[p] << "  " << dOutputSignal[p] << endl;
		else
			cout << "Convolution " << p << " " << "            " << dOutputSignal[p] << endl;

	}
}

void noise_test()
{
	srand((unsigned int)time(NULL));

	double dNoiseValue = 0.0;

	for (unsigned int p = 0; p < 1024; p++)
	{
		norm_dist_noise_1(&dNoiseValue,0.0,1.0);
		cout << "Noise " << p << " :" << dNoiseValue << endl;
	}

	for (unsigned int p = 0; p < 1024; p++)
	{
		norm_dist_noise_2(&dNoiseValue,0.0,1.0);
		cout << "Noise 2 " << p << " :" << dNoiseValue << endl;
	}


	digitizer_noise(&dNoiseValue,8);

	cout << "8 bit digitizer noise: " << dNoiseValue << endl;
}

double dRealX[1024/2 + 1];
double dImagX[1024/2 + 1];

void dft_test()
{
	dft(dRealX,dImagX,dSignal,1024);

	for (int ii = 0; ii < (1024/2 + 1); ii++)
	{
		ofile << "Freq: " << ii << "  real: " << (int)dRealX[ii] << "  imag: " << (int)dImagX[ii] << endl;
	}
}

void dft_reverse_test()
{
	double dInverseSignal[1024];

	dft_inverse_synthesis(dInverseSignal,1024,dRealX,dImagX);

	for (int ii = 0; ii < 1024; ii++)
	{
		ofile << "Sample " << ii <<  "     Original: " << dSignal[ii] << " Inverse: " << dInverseSignal[ii] << endl;	
	}
}

void rectangular_polar_conversion_test()
{
	double dReal = 0.5;
	double dImag = 0.78;

	double dMag		= 0.0;
	double dPhase	= 0.0;

	rectangular_to_polar_conversion(&dMag,&dPhase,dReal,dImag);

	ofile << "Magnitude " << dMag << " Phase " << dPhase << endl;

	double dRealNew = 0.0;
	double dImagNew = 0.0;

	polar_to_rectangular_conversion(&dRealNew,&dImagNew,dMag,dPhase);

	ofile << "Real: " << dRealNew << " Imaginary: " << dImagNew << endl;


}

