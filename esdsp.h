#ifndef __ESDSP_H
#define __ESDSP_H

#include "error.h"

#define PI	3.14159265359
#define EULER_NUMBER	2.7182818284590452353602874713527


int		mean(double *dMean, double *adSample, unsigned int iNumberOfElements);
int		variance(double *dVariance, double *adSample, unsigned int iNumberOfElements);
int		standard_deviation(double *dStdDev, double *adSample, unsigned int iNumberOfElements);
int		mean_stdev_running(double *dMean, double *dStandard_Dev, unsigned int *iNumberOfElements, double *dSum, double *dSumSquares,double dSignal);
int		typical_mean_error(double *dMeanCalcError,double dStandardDeviation, int iNumberOfSamples);
int		histogram_running(unsigned int *aiBins,double dSample,double dMinValue, double dMaxValue,unsigned int iNumberOfBins);
int		normal_distribution(double *adNormalDist, double dRange, double *adSample, unsigned int iNumberElements);
int		norm_dist_noise_1(double *dNoise, double dMean, double dStandard_Dev);
int		norm_dist_noise_2(double *dNoise, double dMean, double dStandard_Dev);
int		digitizer_noise(double *dNoise, int iNumberBits);
int		convolution_input(double *adOutputSignal, double *adSignal, unsigned int iNumOfSigElements, double *adSignal_Response, unsigned int iNumOfResponseElements);
int		convolution_output(double *adOutputSignal, double *adSignal, unsigned int iNumOfSigElements, double *adSignal_Response, unsigned int iNumOfResponseElements);
int		dft(double *adRealOutputSignal, double *adImaginaryOutputSignal, double *adSignal, unsigned int iNumOfSigElements);
int		dft_inverse_synthesis(double *adSignal, unsigned int iNumOfSigElements, double *adRealOutputSignal, double *adImaginaryOutputSignal);
int		rectangular_to_polar_conversion(double *polar_maginute, double *polar_phase, double dReal, double dImag);
int		polar_to_rectangular_conversion(double *dReal, double *dImag,double polar_maginute, double polar_phase);
int		freq_domain_multiplication_rectangular(double *dRealOutput, double *dImOutput, double *adReal1, double *adIm1, double* adReal2, double *adIm2, unsigned int iNumberSamples);
int		freq_domain_division_rectangular(double *dRealOutput, double *dImOutput, double *adReal1, double *adIm1, double* adReal2, double *adIm2, unsigned int iNumberSamples);
int		freq_domain_multiplication_polar(double *dMagnitudeOutput, double *dPhaseOutput, double *adMagnintude1, double *adPhase1, double* adMagnitude2, double *adPhase2, unsigned int iNumberSamples);
int		freq_domain_division_polar(double *dMagnitudeOutput, double *dPhaseOutput, double *adMagnintude1, double *adPhase1, double* adMagnitude2, double *adPhase2, unsigned int iNumberSamples);
#endif