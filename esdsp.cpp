// ESDSP.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "esdsp.h"
#include <math.h>
/*************************************************************/
//Function:		mean
//Description:	compute Mean of signal
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Eq 2-1
//Inputs:		*dMean		- computed mean
//				adSample	- array of signal values
//				iNumberOfElements - number of elements in array adSample
//Outputs:		error
/************************************************************/

int		mean(double *dMean, double *adSample, unsigned int iNumberOfElements)
{
	if (!adSample || !dMean)
		return ESDSP_ERR_NULL_POINTER;
	else if (iNumberOfElements <= 0)
		return ESDSP_ERR_NUM_ELEMENTS;

	unsigned int iElem = 0;
	*dMean = 0;

	for (; iElem < iNumberOfElements; iElem++)
		*dMean+=adSample[iElem];

	*dMean /= iNumberOfElements;

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		variance
//Description:	compute Variance of signal
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Eq 2-2
//Inputs:		*dVariance		- computed variance
//				adSample	- array of signal values
//				iNumberOfElements - number of elements in array adSample
//Outputs:		error
/************************************************************/
int		variance(double *dVariance, double *adSample, unsigned int iNumberOfElements)
{
	double dMean = 0;
	int		iError = 0;

	if (iError = (mean(&dMean,adSample,iNumberOfElements)) != ESDSP_ERR_NO_ERROR)
		return iError;

	if (!dVariance)
		return ESDSP_ERR_NULL_POINTER;
	else if (iNumberOfElements <= 1)
		return ESDSP_ERR_NUM_ELEMENTS;

	unsigned int iElem = 0;
	*dVariance = 0;

	for (; iElem < iNumberOfElements; iElem++)
	{
		double dDiff = (adSample[iElem] -dMean);
		*dVariance+= dDiff * dDiff;
	}

	*dVariance /= (iNumberOfElements - 1);

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		standard_deviation
//Description:	compute standard deviation of signal
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Eq 2-2
//Inputs:		*dStdDev		- computed standard deviation
//				adSample	- array of signal values
//				iNumberOfElements - number of elements in array adSample
//Outputs:		error
/************************************************************/
int		standard_deviation(double *dStdDev, double *adSample, unsigned int iNumberOfElements)
{
	double	dVariance = 0;
	int		iError = 0;

	if (iError = (variance(&dVariance,adSample,iNumberOfElements)) != ESDSP_ERR_NO_ERROR)
		return iError;	

	if (!dStdDev)
		return ESDSP_ERR_NULL_POINTER;

	*dStdDev = sqrt(dVariance);

	if (*dStdDev != *dStdDev)
		*dStdDev = 0;

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		mean_stdev_running
//Description:	compute mean and standard deviation using running statistics
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Eq 2-2
//Inputs:		*dMean				-computed mean
//				*dStdDev			-computed standard deviation
//				*iNumberOfElements	-running number of signals
//				*iSum				-running sum
//				*dSumSquares		-running sum squares	
//				dSignal				- signal value	
//Outputs:		error
/************************************************************/
int		mean_stdev_running(double *dMean, double *dStandard_Dev, unsigned int *iNumberOfElements, double *dSum, double *dSumSquares,double dSignal)
{
	if (!(dMean && dStandard_Dev && iNumberOfElements && dSum && dSumSquares))
		return ESDSP_ERR_NULL_POINTER;
	else if (*iNumberOfElements < 0)
		return ESDSP_ERR_NUM_ELEMENTS;

	*iNumberOfElements+=1;
	(*dSum)+= dSignal;
	(*dSumSquares)+=(dSignal*dSignal);

	(*dMean) = (*dSum)/(*iNumberOfElements);

	double	dVariance = 1.0 / (*iNumberOfElements) * (*dSumSquares - (*dSum) * (*dSum) / *iNumberOfElements);

	(*dStandard_Dev) = pow(dVariance,0.5);

	if (*dStandard_Dev != *dStandard_Dev)
		*dStandard_Dev = 0;

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		typical_mean_error
//Description:	computes typical error in calculating the meain of an underlying
//				process by using a finit number of samples
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Eq 2-4
//Inputs:		dStandardDeviation				-computed mean
//				*dMeanCalcError			- error 
//				dStandardDeviation		- standard deviation
//				iNumberOfSamples		- number of samples
//Outputs:		error
/************************************************************/
int	typical_mean_error(double *dMeanCalcError,double dStandardDeviation, int iNumberOfSamples)
{
	if (!dMeanCalcError)
		return ESDSP_ERR_NULL_POINTER;
	else if (iNumberOfSamples <= 0)
		return ESDSP_ERR_NUM_ELEMENTS;
		
	*dMeanCalcError = dStandardDeviation / pow(iNumberOfSamples,0.5);

	return ESDSP_ERR_NO_ERROR;
}	
/*************************************************************/
//Function:		histogram_running
//Description:	creates histogram in run-time
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Figure 2-4
//Inputs:		aiBins				- histogram
//				dSample				- sample value 
//				dMinValue			- minimum value
//				dMaxValue			- maximum value
//				iNumberOfBins		- number of bins
//Outputs:		error
/************************************************************/
int		histogram_running(unsigned int *aiBins,double dSample,double dMinValue, double dMaxValue,unsigned int iNumberOfBins)
{
	if (iNumberOfBins == 0)
		return ESDSP_ERR_NUM_ELEMENTS;
 
	double dStep = (dMaxValue - dMinValue) / iNumberOfBins;

	if (dSample < dMinValue)
		return ESDSP_ERR_VAL_OUT_OF_BOUND;
	else if (dSample > dMaxValue)
		return ESDSP_ERR_VAL_OUT_OF_BOUND;

	double dOffset = dSample - dMinValue;

	unsigned int iBinNumber;

	double d = dOffset/dStep;

	iBinNumber = (unsigned int)floor(d);

	if (dSample >= dMaxValue)
		iBinNumber--;

	aiBins[iBinNumber]++;

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		normal_distribution
//Description:	creates normal distribution curve for signal
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 2-8
//Inputs:		adNormalDist - pointer to normal distribution curve, each value in array is probability of sample value occurance	
//				dRange - range of all possible values (dMax - dMin)
//				adSample - array containing sample values
//				iNumberElements - number of samples
//Outputs:		error
/************************************************************/
int		normal_distribution(double *adNormalDist, double dRange, double *adSample, unsigned int iNumberElements)
{	
	double dMean, dStandardDeviation;

	int iError = 0;

	iError = mean(&dMean,adSample,iNumberElements);

	if (iError != ESDSP_ERR_NO_ERROR)
		return iError;

	iError =  standard_deviation(&dStandardDeviation,adSample,iNumberElements);

	if (iError != ESDSP_ERR_NO_ERROR)
		return iError;

	if (dStandardDeviation == 0)
	{
		return ESDSP_ERR_VAL_OUT_OF_BOUND;
	}

	unsigned int iNormDistCount = 0;

	for (;iNormDistCount < iNumberElements; iNormDistCount++)
	{
			double dPow = -pow((adSample[iNormDistCount] - dMean),2) / (2.0 * dStandardDeviation * dStandardDeviation);

			adNormalDist[iNormDistCount] = pow(EULER_NUMBER,dPow) / dRange; 	
	}	

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		norm_dist_noise_1
//Description:	generates normally distributed noise value
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 2-10
//Inputs:		dNoise - pointer to noise value computed in this function
//				dMean - desired mean
//				dStandard_Dev - desired standard deviation
//Outputs:		error
/************************************************************/
int		norm_dist_noise_1(double *dNoise, double dMean, double dStandard_Dev)
{
	if (!dNoise)
		return ESDSP_ERR_NULL_POINTER;
	
	double adNoise[12];
	
	int i;

	for (i = 0; i < 12; i++)
		adNoise[i]= (double)rand() / RAND_MAX;

	*dNoise = 0.0;

	for (i = 0; i < 12; i++)
	{
		*dNoise+=adNoise[i];
	}

	*dNoise = (*dNoise - 6) * dStandard_Dev + dMean;
	
	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		norm_dist_noise_2
//Description:	generates normally distributed noise value
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 2-9
//Inputs:		dNoise - pointer to noise value computed in this function
//				dMean - desired mean
//				dStandard_Dev - desired standard deviation
//Outputs:		error
/************************************************************/
int		norm_dist_noise_2(double *dNoise, double dMean, double dStandard_Dev)
{
	if (!dNoise)
		return ESDSP_ERR_NULL_POINTER;
	
	double dR1 = (double)rand()/RAND_MAX;
	double dR2 = (double)rand()/RAND_MAX;

	*dNoise = (pow((-2.0 * log(dR1)),0.5) * cos(2.0 * PI * dR2)) * dStandard_Dev + dMean;
	
	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		digitizer_noise
//Description:	copmutes digitizer noise
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Chapter 3, Quantiziation
//Inputs:		dNoise - pointer to noise value computed in this function
//				iNumberBits - number of digitizer's bits
//Outputs:		error
/************************************************************/
int		digitizer_noise(double *dNoise, int iNumberBits)
{
	//*dNoise = (1/sqrt(12.0))/pow(2.0,iNumberBits);

	*dNoise = 0.288765/pow(2.0,iNumberBits);

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		convolution input
//Description:	perform signal convolultion
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Table 6-1
//Inputs:		adOutputSignal			- buffer for output signal
//				adSignal				- signal to be processed
//				iNumOfSigElements		- size of signals
//				adSignal_Response		- buffer for signal response
//				iNumOfResponseElements	- size of signal response
//Outputs:		error
/************************************************************/
int		convolution_input(double *adOutputSignal, double *adSignal, unsigned int iNumOfSigElements, double *adSignal_Response, unsigned int iNumOfResponseElements)
{
	for (unsigned int ii = 0; ii < iNumOfSigElements; ii++)
	{
		for (unsigned int jj = 0; jj < iNumOfResponseElements; jj++)
		{
			adOutputSignal[ii + jj] = adOutputSignal[ii + jj] + adSignal[ii] * adSignal_Response[jj];
		}
	}
	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		convolution_output
//Description:	perform signal convolultion
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Table 6-2
//Inputs:		adOutputSignal			- buffer for output signal
//				adSignal				- signal to be processed
//				iNumOfSigElements		- size of signals
//				adSignal_Response		- buffer for signal response
//				iNumOfResponseElements	- size of signal response
//Outputs:		error
/************************************************************/
int		convolution_output(double *adOutputSignal, double *adSignal, unsigned int iNumOfSigElements, double *adSignal_Response, unsigned int iNumOfResponseElements)
{
	unsigned int iOutputElements = iNumOfSigElements + iNumOfResponseElements - 1;

	for (unsigned int ii = 0; ii < iOutputElements; ii++)
	{
		adOutputSignal[ii] = 0;

		for (unsigned int jj = 0; jj < iNumOfResponseElements; jj++)
		{
			unsigned int iElem = ii - jj;

			if (iElem < 0 || iElem >= iNumOfSigElements)
				continue;
			else
				adOutputSignal[ii] = adOutputSignal[ii] + adSignal_Response[jj] * adSignal[iElem]; 
		}
	}		

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		Discrete Furier Transform
//Description:	performs DFT, from time to frequency domain
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 8-4
//Inputs		adSignal				- input signal buffer
//				iNumOfSigElements		- number of elements in signal
//Outputs:		adRealOutputSignal		- frequency domain real part	normalized
//				adImaginaryOutputSignal	- frequency domain imaginary part normalized
//Outputs:		error
/************************************************************/
int		dft(double *adRealOutputSignal, double *adImaginaryOutputSignal, double *adSignal, unsigned int iNumOfSigElements)
{

	for (unsigned  int k = 0; k < (iNumOfSigElements / 2 + 1); k++)
	{
		adRealOutputSignal[k] = 0;
		adImaginaryOutputSignal[k] = 0;

		for (unsigned int i = 0; i < iNumOfSigElements; i++)
		{
			adRealOutputSignal[k]		+=	adSignal[i] * cos(2 * PI * k * i / iNumOfSigElements);
			adImaginaryOutputSignal[k]	-=	adSignal[i] * sin(2 * PI * k * i / iNumOfSigElements);
		}
	}

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		Inverse Discrete Fourier Transform (Synthesis)
//Description:	performs inverse DFT, from frequency to time domain
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 8-4
//Outputs:		adRealOutputSignal		- frequency domain real part	normalized
//				adImaginaryOutputSignal	- frequency domain imaginary part normalized
//				adSignal				- input signal buffer
//				iNumOfSigElements		- number of elements in signal
//
//Outputs:		error
/************************************************************/
int dft_inverse_synthesis(double *adSignal, unsigned int iNumOfSigElements, double *adRealOutputSignal, double *adImaginaryOutputSignal)
{
	for (unsigned int i = 0; i < iNumOfSigElements; i++)
	{
			adSignal[i] = 0;

			for (unsigned k = 0; k < (iNumOfSigElements / 2 + 1); k++)
			{
				double d = (k == 0 || k == (iNumOfSigElements / 2)) ? iNumOfSigElements : iNumOfSigElements / 2;

				adSignal[i] += adRealOutputSignal[k] / d * cos (2.0 * PI * k * i / iNumOfSigElements) - adImaginaryOutputSignal[k]/d * sin(2 * PI * k * i / iNumOfSigElements);
			}
	}

	return ESDSP_ERR_NO_ERROR;
}

/*************************************************************/
//Function:		rectangular_to_polar_conversion
//Description:	Converts Rectangular representaiton in freq domain
//				to polar representation
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 8-6
//Inputs		double dReal			-	real value
//				double dImaginary		-	imaginary value
//Outputs:		double polar_maginute	-	polar maginute
//				polar_phase				-	 polar phase
//Outputs:		error
/************************************************************/
int		rectangular_to_polar_conversion(double *polar_maginute, double *polar_phase, double dReal, double dImag)
{
	if (!polar_maginute || !polar_phase)
		return ESDSP_ERR_NULL_POINTER;
	
	*polar_maginute = pow(dReal * dReal + dImag * dImag, 0.5);
	*polar_phase	= atan2(dImag,dReal);


	return ESDSP_ERR_NO_ERROR;
}

/*************************************************************/
//Function:		polar_to_rectangular_conversion
//Description:	COnverts polar to rectangular representation
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 8-7
//Outputs		double dReal			-	real value
//				double dImaginary		-	imaginary value
//Inputs:		double polar_maginute	-	polar maginute
//				polar_phase				-	 polar phase
//Outputs:		error
/************************************************************/
int		polar_to_rectangular_conversion(double *dReal, double *dImag,double polar_maginute, double polar_phase)
{
	if (!dReal || !dImag)
		return ESDSP_ERR_NULL_POINTER;
	
	*dReal = polar_maginute * cos(polar_phase);
	*dImag = polar_maginute * sin(polar_phase);

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		freq_domain_multiplication_rectangular
//Description:	Multiplication in frequency domain using rectangular form
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 9-1
//Inputs:		dRealOutput			- buffer contains real part of multiplication
//				dImOutput			- buffer contains imaginary part of multiplication
//				adReal1				- buffer real part of input signal 1 to be multiplied
//				adIm1				- buffer imaginary part of input signal 1 to be multiplied
//				adReal2				- buffer real part of input signal 2 multiplier
//				adIm2				- buffer imaginary part of input signal 2 multiplier
//				iNumberSamples		- number of samples in each buffer (number of frequencies in frequency domain spectrum)
//Outputs:		error
/************************************************************/
int freq_domain_multiplication_rectangular(double *dRealOutput, double *dImOutput, double *adReal1, double *adIm1, double* adReal2, double *adIm2, unsigned int iNumberSamples)
{
	if (!dRealOutput || !dImOutput || !adReal1 || !adIm1 || !adReal2 || !adIm2)
		return ESDSP_ERR_NULL_POINTER;

	for (unsigned int ii = 0; ii < iNumberSamples; ii++)
	{
		dRealOutput[ii] = adReal1[ii] * adReal2[ii] - adIm1[ii] * adIm2[ii];
		dImOutput[ii]	= adIm1[ii] * adReal2[ii] + adReal1[ii] * adIm2[ii];
	}

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		freq_domain_division_rectangular
//Description:	Multiplication in frequency domain using rectangular form
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 9-1
//Inputs:		dRealOutput			- buffer contains real part of division
//				dImOutput			- buffer contains imaginary part of division
//				adReal1				- buffer real part of input signal 1 to be divided
//				adIm1				- buffer imaginary part of input signal 1 to be divided
//				adReal2				- buffer real part of input signal 2 divisor
//				adIm2				- buffer imaginary part of input signal 2 divisor
//				iNumberSamples		- number of samples in each buffer (number of frequencies in frequency domain spectrum)
//Outputs:		error
/************************************************************/
int freq_domain_division_rectangular(double *dRealOutput, double *dImOutput, double *adReal1, double *adIm1, double* adReal2, double *adIm2, unsigned int iNumberSamples)
{
	if (!dRealOutput || !dImOutput || !adReal1 || !adIm1 || !adReal2 || !adIm2)
		return ESDSP_ERR_NULL_POINTER;

	for (unsigned int ii = 0; ii < iNumberSamples; ii++)
	{
		double dD = adReal2[ii] * adReal2[ii] + adIm1[ii] * adIm1[ii];

		if (dD == 0)
		{
			dRealOutput[ii] = 
			dImOutput[ii]	= 0;
		}
		else
		{
			dRealOutput[ii] = (adReal1[ii] * adReal2[ii] + adIm1[ii]*adIm2[ii])/dD;
			dImOutput[ii]	= (adIm1[ii] * adReal2[ii] - adIm1[ii] * adIm2[ii])/dD;
		}
	}

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		freq_domain_multiplication_polar
//Description:	Multiplication in frequency domain using rectangular form
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 9-1
//Inputs:		dMagnitudeOutput	- buffer contains magnitude of multiplication of frequency domains
//				dPhaseOutput		- buffer contains phase of multiplication of frequency domains
//				adMagnintude1		- buffer magnitude of input signal 1 to be multiplied
//				adPhase1			- buffer phase of input signal 1 to be multiplied
//				adPhase2			- buffer magnitude of input signal 2 multiplier
//				adIm2				- buffer phase of input signal 2 multiplier
//				iNumberSamples		- number of samples in each buffer (number of frequencies in frequency domain spectrum)
//Outputs:		error
/************************************************************/
int freq_domain_multiplication_polar(double *dMagnitudeOutput, double *dPhaseOutput, double *adMagnintude1, double *adPhase1, double* adMagnitude2, double *adPhase2, unsigned int iNumberSamples)
{
	if (!dMagnitudeOutput || !dPhaseOutput || !adMagnintude1 || !adPhase1 || !adMagnitude2 || !adPhase2)
		return ESDSP_ERR_NULL_POINTER;

	for (unsigned int ii = 0; ii < iNumberSamples; ii++)
	{
		dMagnitudeOutput[ii] = adMagnintude1[ii] * adMagnitude2[ii];
		dPhaseOutput[ii]	= adPhase1[ii] + adPhase2[ii];
	}

	return ESDSP_ERR_NO_ERROR;
}
/*************************************************************/
//Function:		freq_domain_division_polar
//Description:	Multiplication in frequency domain using rectangular form
//				
//Source:		The Scientists and Engineer's Guide to Digital Signal 
//				Processing, Steven W.Smith, Ph.D, Equation 9-1
//Inputs:		dMagnitudeOutput	- buffer contains magnitude of division of frequency domains
//				dPhaseOutput		- buffer contains phase of division of frequency domains
//				adMagnintude1		- buffer magnitude of input signal 1 to be dividied
//				adPhase1			- buffer phase of input signal 1 to be dividied
//				adPhase2			- buffer magnitude of input signal 2 divisor
//				adIm2				- buffer phase of input signal 2 divisor
//				iNumberSamples		- number of samples in each buffer (number of frequencies in frequency domain spectrum)
//Outputs:		error
/************************************************************/
int freq_domain_division_polar(double *dMagnitudeOutput, double *dPhaseOutput, double *adMagnintude1, double *adPhase1, double* adMagnitude2, double *adPhase2, unsigned int iNumberSamples)
{
	if (!dMagnitudeOutput || !dPhaseOutput || !adMagnintude1 || !adPhase1 || !adMagnitude2 || !adPhase2)
		return ESDSP_ERR_NULL_POINTER;

	for (unsigned int ii = 0; ii < iNumberSamples; ii++)
	{
		dMagnitudeOutput[ii] = adMagnintude1[ii] / adMagnitude2[ii];
		dPhaseOutput[ii]	= adPhase1[ii] - adPhase2[ii];
	}

	return ESDSP_ERR_NO_ERROR;
}

