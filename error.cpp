
#include "stdafx.h"
#include "error.h"

char*	errors[] = 
{
	"Not an error.",					//0
	"Error not found.",					//-1
	"Number of elements is incorrect.",	//-2
	"Null Pointer"						//-3
};

char*	error_explain(int iError)
{
	switch(iError)
	{
	case	ESDSP_ERR_NO_ERROR:
	case	ESDSP_ERR_NOT_FOUND:
	case	ESDSP_ERR_NUM_ELEMENTS:
	case	ESDSP_ERR_NULL_POINTER:
		return errors[abs(iError)];
		break;
	default:
		return errors[abs(ESDSP_ERR_NOT_FOUND)];
	}
}